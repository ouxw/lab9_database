/* begin table creation */

CREATE DATABASE astro;

USE astro;

CREATE table photometry (
    ob_id BIGINT UNSIGNED,
    ra FLOAT,
    de FLOAT,
    magr FLOAT,
    PRIMARY KEY (ob_id)
    );

CREATE table spectroscopy (
    ob_id BIGINT UNSIGNED,
    ra FLOAT,
    de FLOAT,
    z FLOAT,
    PRIMARY KEY (ob_id)
    );

CREATE table qsos (
    ob_id BIGINT UNSIGNED,
    ra FLOAT,
    de FLOAT,
    z FLOAT,
    PRIMARY KEY (ob_id)
    );


/* end table creation */

/* begin data population */

LOAD DATA
    INFILE "/tmp/Galaxies_photometry.csv"
    INTO TABLE photometry
    FIELDS TERMINATED BY ","
    (ob_id, ra, de, magr);

LOAD DATA
    INFILE "/tmp/Galaxies_spectroscopy.csv"
    INTO TABLE spectroscopy
    FIELDS TERMINATED BY ","
    (ob_id, ra, de, z);

LOAD DATA
    INFILE "/tmp/QSOs_spectroscopy.csv"
    INTO TABLE qsos
    FIELDS TERMINATED BY ","
    (ob_id, ra, de, z);
/* end data population */

/* begin joining data and output */

SELECT q.ob_id, q.ra, q.de, q.z, s.z, p.magr
    FROM qsos q
    JOIN photometry p ON ABS(p.ra-q.ra) <= 0.0002 AND ABS(p.de-q.de) <= 0.0002
    JOIN spectroscopy s ON ABS(s.ra-q.ra) <= 0.0002 AND ABS(s.de-q.de) <= 0.0002
    INTO OUTFILE '/tmp/pos_base.txt';

SELECT q.ob_id, q.ra, q.de, q.z, s.z, p.magr
    FROM qsos q
    JOIN photometry p ON q.ob_id=p.ob_id
    JOIN spectroscopy s ON q.ob_id=s.ob_id
    INTO OUTFILE '/tmp/id_base.txt';
